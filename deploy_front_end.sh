#!/bin/bash

SITE_URL="anorak.games.archin.al"
AWS_PROFILE=$1
AWS_CLOUDFRONT_DISTRIBUTION_ID="E3MMMJ5EDBPA7D"

PROFILE_STR=""
if [[ $# -gt 0 ]]; then
    PROFILE_STR="--profile ${AWS_PROFILE}"
fi

cd `dirname $0`

rm -r build/

npm run build

aws s3 sync build/ s3://${SITE_URL}/ --delete ${PROFILE_STR}

aws cloudfront create-invalidation --distribution-id ${AWS_CLOUDFRONT_DISTRIBUTION_ID} --paths "/*" ${PROFILE_STR}
