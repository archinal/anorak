import {DEFAULT_VERBS} from "./defaultVerbs";

export default {
    [DEFAULT_VERBS.HELP]: () => ({
        logEntry:
`To play this game you need to type simple commands on the command line.

You can enter simple commands like **\`look around\`**, **\`take sword\`** or **\`hit tree with axe\`** to interact with the environment.`
    }),

    [DEFAULT_VERBS.ABOUT]: () => ({
        logEntry: `
        This game was written on the DCLIG engine, created by William Archinal.
        `
    }),
}