import get from 'lodash/get';
import has from 'lodash/has';
import merge from 'lodash/merge';
import {DEFAULT_VERBS, DEFAULT_VERB_DICTIONARY} from "./defaultVerbs";
import defaultActions from "./defaultActions";

const DEFAULT_SCENE_RESULT = {
    newScene: null,
    logEntry: 'Nothing happens'
};

export default class DCLIG {
    constructor({
                    state = {},
                    verbs = {},
                    objects = {},
                    verbDictionary = {},
                    objectDictionary = {},
                    log = [],
                    error = null,
                    title = '',
                    currentScene = null,
                    scenes = {},
                    inventory = {},
                    inventoryActions = {},
                    customDefaultActions = {},
                    introduction = '',
                }) {
        Object.assign(this, {
            state,
            verbs: merge({}, verbs, DEFAULT_VERBS),
            objects,
            verbDictionary: merge({}, verbDictionary, DEFAULT_VERB_DICTIONARY),
            objectDictionary,
            log,
            error,
            title,
            scenes,
            inventory,
            inventoryActions,
            customDefaultActions
        });
        if (introduction) {
            this.log.push(introduction);
        }
        this.currentScene = currentScene;
    }

    handleCommand = (str) => {
        const {valid, object, verb, indirectObject} = this.parseCommand(str);
        if (valid) {
            let actionFnPath = verb;
            if (object !== null) {
                actionFnPath += `.${object}`;
                if (indirectObject !== null) {
                    actionFnPath += `.${indirectObject}`;
                }
            }
            // TODO check scene, then inventory, then defaults
            let actionFn = null;
            if (has(this.currentScene.actions, actionFnPath)) {
                actionFn = get(this.currentScene.actions, actionFnPath);
            } else if (has(this.inventoryActions, actionFnPath)) {
                actionFn = get(this.inventoryActions, actionFnPath);
            } else if (has(this.customDefaultActions, actionFnPath)) {
                actionFn = get(this.customDefaultActions, actionFnPath);
            } else if (has(defaultActions, actionFnPath)) {
                actionFn = get(defaultActions, actionFnPath);
            }
            if (actionFn && typeof actionFn === 'function') {
                const {newScene, logEntry} = Object.assign({}, DEFAULT_SCENE_RESULT, actionFn(this.state, this.inventory));
                this.log.push(logEntry);
                if (newScene) {
                    this.currentScene = newScene;
                }
            } else {
                const {logEntry} = DEFAULT_SCENE_RESULT;
                this.log.push(logEntry);
            }

            this.error = null;
        } else {
            this.error = `Input command not understood ("${str}"). Type "help" for instructions.`
        }
    };

    actionCommand = () => {
        throw new Error('Abstract method called');
    };

    parseCommand = (str) => {
        const upperStr = str.toUpperCase();
        const commandArr = upperStr.split(' ');

        let verb = null;
        let object = null;
        let indirectObject = null;
        let valid = true;

        if (commandArr.length === 0) {
            valid = false;
        }
        if (commandArr.length >= 1) {
            verb = this.getVerb(commandArr[0]);
            if (!verb) {
                valid = false;
            }
        }
        if (commandArr.length >= 2) {
            let index = 1;
            if (commandArr.length <= 3) {
                index = Math.min(2, commandArr.length - 1);
            }
            object = this.getObject(commandArr[index]);
            if (!object) {
                valid = false;
            }
        }
        if (commandArr.length >= 4) {
            indirectObject = this.getObject(commandArr[Math.min(4, commandArr.length - 1)]);
            if (!indirectObject) {
                valid = false;
            }
        }

        return {
            verb,
            object,
            indirectObject,
            valid,
        }
    };

    isFinished = () => false;

    getVerb(verb) {
        return this.verbDictionary[verb] || null;
    }

    getObject(object) {
        return this.objectDictionary[object] || null;
    }

    get currentScene() {
        return this._currentScene;
    }

    set currentScene(scene) {
        this.log.push(scene.introduction);
        this._currentScene = scene;
    }
}