import reduce from 'lodash/reduce';
import keyMirror from 'key-mirror';

export const DEFAULT_VERBS = keyMirror({
    HELP: null,
    ABOUT: null,
});

export const DEFAULT_REVERSE_VERB_DICTIONARY = {
    [DEFAULT_VERBS.HELP]: ['help'],
    [DEFAULT_VERBS.ABOUT]: ['about', 'info'],
};

export const DEFAULT_VERB_DICTIONARY = reduce(DEFAULT_REVERSE_VERB_DICTIONARY, (curr, synonyms, verb) => {
    synonyms.forEach((synonym) => {
        curr[synonym.toUpperCase()] = verb;
    });
    return curr;
}, {});