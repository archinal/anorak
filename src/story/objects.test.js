import flatten from 'lodash/flatten';
import values from 'lodash/values';
import {OBJECTS, OBJECT_DICTIONARY, REVERSE_OBJECT_DICTIONARY} from "./objects";

describe('object tests', () => {
    it('object dictionary has no duplicate synonyms', () => {
        const allGivenSynonyms = flatten(values(REVERSE_OBJECT_DICTIONARY));
        expect(Object.keys(OBJECT_DICTIONARY).length).toEqual(allGivenSynonyms.length);
    });

    it('has synonyms for all objects', () => {
        expect(Object.keys(REVERSE_OBJECT_DICTIONARY).length).toEqual(Object.keys(OBJECTS).length);
    });
});