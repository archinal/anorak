import reduce from 'lodash/reduce';
import keyMirror from 'key-mirror';

export const OBJECTS = keyMirror({
    WAND: null,
    RING: null,
    BOULDERS: null,
    TUNNEL: null,
    AROUND: null,
    WATER: null,
    LANDING: null,
    FOREST: null,
    PATH: null,
});

export const REVERSE_OBJECT_DICTIONARY = {
    [OBJECTS.WAND]: ['wand'],
    [OBJECTS.RING]: ['ring'],
    [OBJECTS.AROUND]: ['around', 'about', 'surroundings'],
    [OBJECTS.WATER]: ['water', 'pool'],
    [OBJECTS.LANDING]: ['landing', 'ground', 'shore'],
    [OBJECTS.BOULDERS]: ['boulders', 'rocks', 'cave-in', 'boulder'],
    [OBJECTS.TUNNEL]: ['tunnel', 'ledge', 'opening'],
    [OBJECTS.FOREST]: ['forest', 'trees'],
    [OBJECTS.PATH]: ['path', 'road'],
};

export const OBJECT_DICTIONARY = reduce(REVERSE_OBJECT_DICTIONARY, (curr, synonyms, object) => {
    synonyms.forEach((synonym) => {
        curr[synonym.toUpperCase()] = object;
    });
    return curr;
}, {});