import flatten from 'lodash/flatten';
import values from 'lodash/values';
import {VERBS, VERB_DICTIONARY, REVERSE_VERB_DICTIONARY} from "./verbs";

describe('verb tests', () => {
    it('verb dictionary has no duplicate synonyms', () => {
        const allGivenSynonyms = flatten(values(REVERSE_VERB_DICTIONARY));
        expect(Object.keys(VERB_DICTIONARY).length).toEqual(allGivenSynonyms.length);
    });

    it('has synonyms for all verbs', () => {
        expect(Object.keys(REVERSE_VERB_DICTIONARY).length).toEqual(Object.keys(VERBS).length);
    });
});