import {VERBS} from '../verbs';
import {OBJECTS} from '../objects';
import get from 'lodash/get';

export default {
    [VERBS.EXAMINE]: {
        [OBJECTS.WAND]: (state, inventory) => {
            if (get(inventory, `${OBJECTS.WAND}.quantity`, 0) >= 1) {
                return {
                    logEntry: `
This small oak wand is used to magically lift and move heavy objects.

It has been known to get wizards out of many a precarious situation.
`
                }
            }
        },
        [OBJECTS.RING]: (state, inventory) => {
            if (get(inventory, `${OBJECTS.RING}.quantity`, 0) >= 1) {
                return {
                    logEntry: `
This small golden ring emits a soft but powerful glow. It belongs to the court wizard,
but is often lent to magicians and wizards who work beneath the tunnels of the city.
`
                }
            }
        }
    },
}