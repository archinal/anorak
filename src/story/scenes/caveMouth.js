import set from 'lodash/set';

import {VERBS} from "../verbs";
import {OBJECTS} from "../objects";

const introduction = `
You find yourself at the end of a tunnel that leads to the mouth of the cave.

The evening sun beats down on your face as the last slivers of daylight start to fade away.

The mouth of the cave overlooks a dense **forest**.
`;

const caveMouth = {
    introduction,
    actions: {
        [VERBS.EXAMINE]: {
            [OBJECTS.AROUND]: () => ({logEntry: introduction}),
            [OBJECTS.FOREST]: () => {
                return {
                    logEntry: `
The **forest** is extremely dense, but just beyond it you can make out what looks like a **path**.
`
                }
            },
            [OBJECTS.PATH]: () => {
                return {
                    logEntry: "The **path** is an old one, but it seems to lead in the direction of the town."
                }
            }
        },
        [VERBS.GO]: {
            [OBJECTS.PATH]: (state) => {
                state.finished = true;
                return {
                    logEntry: `
Just as night begins to fall you stumble onto the **path** and run toward the town.

You're lucky to have escaped the cave-in with your life. Perhaps you'll be more careful in the future... perhaps not.
`
                }
            }
        }
    },
};

set(caveMouth, `actions.${VERBS.GO}.${OBJECTS.FOREST}`, caveMouth.actions[VERBS.EXAMINE][OBJECTS.FOREST]);

export default caveMouth;