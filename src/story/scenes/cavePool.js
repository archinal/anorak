import get from 'lodash/get';
import set from 'lodash/set';

import caveMouth from './caveMouth';

import {VERBS} from "../verbs";
import {OBJECTS} from "../objects";

const introduction = `
You're in a dark cavern. Behind you a mountain of **boulders**, stones, and dirt blocks the passage you entered from.

You can faintly see that the edge of the small **landing** drops into a glossy black **pool** of water about 12 inches below.
`;

const cavePool = {
    introduction,
    actions: {
        [VERBS.EXAMINE]: {
            [OBJECTS.WATER]: (state, inventory) => {
                const hasRing = get(inventory, `${OBJECTS.RING}.quantity`, 0) > 0;
                const poolFilled = get(state, 'cavePool.poolFilled', false);
                let logEntry = hasRing ? `
Using your ring you can see that the pool of water extends about 5 feet across.` : `
In the darkness you can't see much beyond the pool of water, but you can see the faint glow of your magic **ring** at the bottom of the pool, which appears to be about 6 feet deep.
`;

                if (hasRing) {
                    if (poolFilled) {
                        logEntry += `

Now that the pool has been filled, its surface aligns with the entrance to a new tunnel on the other side.
`
                    } else {
                        logEntry += `

On the other side of the pool there appears to be a new **tunnel**, but its mouth sits 3 feet above the surface of the pool - too high to climb into.
`
                    }
                }

                return {
                    logEntry
                }
            },
            [OBJECTS.BOULDERS]: (state, inventory) => {
                const poolFilled = get(state, 'cavePool.poolFilled', false);
                const hasRing = get(inventory, `${OBJECTS.RING}.quantity`, 0) > 0;
                let logEntry;

                if (hasRing) {
                    if (poolFilled) {
                        logEntry = `
Even now that the large **boulders** rest at the bottom of the **pool**, the **tunnel** remains blocked. There must be another way out.
`;
                    } else {
                        logEntry = `
With the light provided by your **ring** you can see the the cave-in consists of many enormous **boulders**,
and even more smaller stones, and dirt. It would take you a lifetime of work to unblock the **tunnel**.
`;
                    }
                } else {
                    logEntry = `
In the darkness you're unable to see much of the cave-in, but by feeling it you can tell that it's made up of many large **boulders**, much too heavy to lift on your own.
`
                }

                return {
                    logEntry
                }
            },
            [OBJECTS.LANDING]: (state, inventory) => {
                const poolFilled = get(state, 'cavePool.poolFilled', false);
                const hasRing = get(inventory, `${OBJECTS.RING}.quantity`, 0) > 0;
                const hasWand = get(inventory, `${OBJECTS.WAND}.quantity`, 0) > 0;
                let logEntry;
                if (hasRing) {
                    if (hasWand && poolFilled) {
                        logEntry = `
Now that the **boulders** have been dropped into the **pool**, the surface of the **pool** laps over the edge of the **landing**.
`;
                    } else {
                        logEntry = `
Your **ring** illuminates the small **landing**. You can see the caved in **tunnel** behind you, and the **pool** in front of you.
${hasWand ? `
There doesn't seem to be anything else on the ground.
` : `
You can also see your small oak **wand** on the ground. You must have dropped it in the cave in.
`}`
                    }
                } else {
                    logEntry = "In the darkness you're unable to see much of the **landing**. It seems to drop off into a body of **water**, with a drop of about 3 feet."
                }

                return {
                    logEntry
                }
            },
            [OBJECTS.AROUND]: (state, inventory) => {
                let logEntry = introduction;
                const hasRing = get(inventory, `${OBJECTS.RING}.quantity`, 0) > 0;

                if (hasRing) {
                    logEntry += `
On the other side of the pool there appears to be a new **tunnel**, but its mouth sits 3 feet above the surface of the pool - too high to climb into.
`
                }

                return {logEntry};
            },
        },
        [VERBS.GO]: {
            [OBJECTS.WATER]: (state, inventory) => {
                const hasRing = get(inventory, `${OBJECTS.RING}.quantity`, 0) > 0;

                let logEntry;
                const inWater = get(state, 'cavePool.inWater', false);
                if (inWater) {
                    logEntry = "You're already in the water.";
                } else {
                    logEntry = `
You dive into the water. Your splash makes an echo all over the cavern, and the water chills you to your bones.${hasRing ? `` : `

At the bottom of the pool you can make our your magic **ring** as it emits a glowing light.`}`;
                }
                set(state, 'cavePool.inWater', true);
                return {
                    logEntry
                };
            },
            [OBJECTS.LANDING]: (state) => {
                let logEntry;
                const inWater = get(state, 'cavePool.inWater', false);
                if (inWater) {
                    logEntry = "You climb out onto the **landing**, dripping water.";
                } else {
                    logEntry = "You're already on the **landing**.";
                }
                set(state, 'cavePool.inWater', false);
                return {
                    logEntry
                };
            },
            [OBJECTS.TUNNEL]: (state) => {
                const inWater = get(state, 'cavePool.inWater', false);
                const poolFilled = get(state, 'cavePool.poolFilled', false);
                let logEntry;
                let newScene = null;
                if (inWater) {
                    if (poolFilled) {
                        logEntry = `
You stand on the large **boulders** you dropped into the **pool**.
With your new height you're just able to reach the ledge, and you climb onto it, and make your way through the dark **tunnel** leading away from the **pool**.

Maybe this will lead to a way out...
`;
                        newScene = caveMouth;
                    } else {
                        logEntry = "You're unable to reach the ledge that leads to the **tunnel**. It's too high up."
                    }
                } else {
                    logEntry = "From this side of the **pool** you're unable to reach the ledge that leads to the **tunnel**."
                }
                return {
                    logEntry,
                    newScene,
                }
            }
        },
        [VERBS.TAKE]: {
            [OBJECTS.RING]: (state, inventory) => {
                const hasRing = get(inventory, `${OBJECTS.RING}.quantity`, 0) > 0;
                const inWater = get(state, 'cavePool.inWater', false);
                let logEntry;
                if (!hasRing) {
                    if (inWater) {
                        logEntry = "You dive down to the bottom of the pool and grab the **ring**, putting it on your finger.";
                        set(inventory, OBJECTS.RING, {
                            name: 'Ring of light',
                            quantity: 1,
                        });
                    } else {
                        logEntry = "You can't reach the bottom of the **pool** from up here."
                    }
                    return {
                        logEntry
                    };
                }
            },
            [OBJECTS.WAND]: (state, inventory) => {
                const hasWand = get(inventory, `${OBJECTS.WAND}.quantity`, 0) > 0;
                const hasRing = get(inventory, `${OBJECTS.RING}.quantity`, 0) > 0;
                const inWater = get(state, 'cavePool.inWater', false);
                if (!hasWand && hasRing) {
                    let logEntry;
                    if (inWater) {
                        logEntry = "You can't reach the **wand** from here."
                    } else {
                        logEntry = "You pick up the **wand**, and enjoy its familiar feeling in your hand."
                        set(inventory, OBJECTS.WAND, {
                            name: 'Oak wand of levitation',
                            quantity: 1,
                        });
                    }
                    return {logEntry};
                }
            },
        },
        [VERBS.USE]: {
            [OBJECTS.WAND]: {
                [OBJECTS.BOULDERS]: (state, inventory) => {
                    const hasWand = get(inventory, `${OBJECTS.WAND}.quantity`, 0) > 0;
                    const poolFilled = get(state, 'cavePool.poolFilled', false);

                    if (hasWand) {
                        let logEntry;
                        if (poolFilled) {
                            logEntry = "You've already moved the large **boulders** into the bottom of the pool."
                        } else {
                            logEntry = "You wave your **wand** and lift the heavy **boulders**, dropping them into the bottom of the shallow **pool**."
                            set(state, 'cavePool.poolFilled', true);
                        }
                        return {logEntry};
                    }
                }
            }
        }
    },
};

set(cavePool, `actions.${VERBS.EXIT}.${OBJECTS.WATER}`, cavePool.actions[VERBS.GO][OBJECTS.LANDING]);

export default cavePool;