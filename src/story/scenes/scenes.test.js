import forEach from 'lodash/forEach';
import scenes from '../scenes';

describe('scene tests', () => {
    it('scenes are valid', () => {
        forEach(scenes, (scene) => {
            expect(scene.introduction).not.toEqual('');
            expect(scene.actions).toBeInstanceOf(Object);
        });
    });
});