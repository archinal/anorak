import cavePool from './cavePool';
import caveMouth from './caveMouth';

export default {
    cavePool,
    caveMouth,
};