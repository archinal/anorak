import reduce from 'lodash/reduce';
import keyMirror from 'key-mirror';

export const VERBS = keyMirror({
    EXAMINE: null,
    GO: null,
    TAKE: null,
    USE: null,
    EXIT: null,
});

export const REVERSE_VERB_DICTIONARY = {
    [VERBS.EXAMINE]: ['look', 'examine', 'check', 'investigate', 'search'],
    [VERBS.GO]: ['walk', 'go', 'run', 'travel', 'jump', 'enter', 'dive'],
    [VERBS.TAKE]: ['take', 'snatch', 'steal', 'pick', 'choose', 'grab'],
    [VERBS.USE]: ['use', 'cast', 'wave'],
    [VERBS.EXIT]: ['exit', 'leave'],
};

export const VERB_DICTIONARY = reduce(REVERSE_VERB_DICTIONARY, (curr, synonyms, verb) => {
    synonyms.forEach((synonym) => {
        curr[synonym.toUpperCase()] = verb;
    });
    return curr;
}, {});