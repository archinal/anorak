import get from 'lodash/get';

import DCLIG from "../engine/DCLIG";
import {VERBS, VERB_DICTIONARY} from "./verbs";
import {OBJECT_DICTIONARY, OBJECTS} from "./objects";
import scenes from './scenes';
import inventoryActions from './inventory/actions';

const introduction = `
Welcome to Anorak, a simple game that explores the features of the DCLIG engine.

To play this game, enter commands in the box below. If you haven't played before, you might like to enter **\`help\`** first.

You play as **Anorak**, a wizard's apprentice trapped in the caves beneath the city of **Valtheim**.

While on an expedition studying the properties of local herbs and fungi, one of the tunnels that runs beneath
the castle caved in. You find yourself stranded on a small landing overlooking the glassy surface of a body of water. 

When the cave collapsed you dropped your wand and your magic ring, and you find yourself lost without them.

You'll need to gather your things, and perhaps more treasures, if you wish to escape the caves before night falls
and the monsters of the night begin to roam the tunnels.
`;

class Anorak extends DCLIG {
    constructor() {

        super({
            verbs: VERBS,
            objects: OBJECTS,
            verbDictionary: VERB_DICTIONARY,
            objectDictionary: OBJECT_DICTIONARY,
            title: 'Anorak\'s Escape',
            scenes,
            introduction,
            inventoryActions,
            currentScene: scenes.cavePool,
        });
    }

    isFinished = () => get(this.state, 'finished', false);
}

export default Anorak;