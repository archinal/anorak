import React, {Component} from 'react';
import takeRight from 'lodash/takeRight';
import Anorak from '../../story/anorak';

import Cli from './cli';
import StoryPage from './storypage';
import './index.css';
import InventoryList from "./inventoryList";

const MAX_LOG_ITEMS = 50;

class Gameboard extends Component {
    constructor() {
        super();
        this.state = {
            log: [],
            inventory: {},
            error: null,
            finished: false,
        };
    }

    componentDidMount() {
        this.game = new Anorak();
        this.updateState();
    }

    updateState = () => {
        this.setState({
            log: takeRight(this.game.log, MAX_LOG_ITEMS),
            error: this.game.error,
            inventory: this.game.inventory,
            finished: this.game.isFinished(),
        });
    };

    handleGameCommandSubmit = (commandStr) => {
        this.game.handleCommand(commandStr);
        this.updateState();
    };

    render() {
        return (
            <div className='gameboard'>
                <div className='gameboard_story-page-container'>
                    <div className='gameboard_story-page-container--story'>
                        <StoryPage log={this.state.log}/>
                    </div>
                    <div className='gameboard_story-page-container--inventory'>
                        <InventoryList inventory={this.state.inventory}/>
                    </div>
                </div>
                {this.state.finished ? (
                    <div>
                        <p>Game over.</p>
                    </div>
                ) : (
                    <div className='gameboard_cli-container'>
                        <div className='gameboard_cli--error'>
                            <p>{this.state.error}</p>
                        </div>
                        <Cli onSubmitCommand={this.handleGameCommandSubmit}/>
                    </div>
                )}
            </div>
        )
    }
}

export default Gameboard;