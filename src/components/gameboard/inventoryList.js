import React from 'react';
import map from 'lodash/map';
import './inventoryList.css';

const InventoryList = ({inventory = {}}) => (
    <div>
        <h3>Inventory</h3>
        {Object.keys(inventory).length > 0 ? (
            <table className='inventory-list'>
                <tbody>
                {map(inventory, ({name, quantity}, id) => quantity > 0 && (
                    <tr key={`inventory-${id}`} className='inventory-list_item'>
                        <td>{name}</td>
                        <td style={{textAlign: 'right'}}>x {quantity}</td>
                    </tr>
                ))}
                </tbody>
            </table>
        ) : (
            <p>You have no items.</p>
        )}

    </div>
);

export default InventoryList;