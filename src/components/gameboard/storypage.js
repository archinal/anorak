import React, {Component} from 'react';
import propTypes from 'prop-types';
import ReactMarkdown from 'react-markdown';
import './storypage.css'

class StoryPage extends Component {
    render() {
        const {log} = this.props;
        return (
            <ul className='story-page'>
                {log.map((entry, index) => (
                    <li className='story-page__list-item' key={index}>
                        <ReactMarkdown source={entry}/>
                    </li>
                ))}
                <li ref={(el) => { this.logsEnd = el; }} />
            </ul>
        );
    }

    scrollToBottom = () => {
        if (this.logsEnd && this.logsEnd.scrollIntoView) {
            this.logsEnd.scrollIntoView({behavior: "smooth"});
        }
    };

    componentDidMount() {
        this.scrollToBottom();
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    static propTypes = {
        log: propTypes.arrayOf(propTypes.string),
    };

    static defaultProps = {
        log: [],
    };
}

export default StoryPage;