import React, {Component} from 'react';
import propTypes from 'prop-types';
import './cli.css';

class Cli extends Component {
    submit = (e, val) => {
        if (this.textInput && this.textInput.value) {
            this.props.onSubmitCommand(this.textInput.value);
            this.textInput.value = '';
        }
    };

    componentDidMount() {
        this.textInput.focus();
    }

    render() {
        return (
            <div className='game-cli'>
                <input
                    type="text"
                    ref={(input) => { this.textInput = input; }}
                    onKeyDown={(event) => {
                        if (event.keyCode === 13) {
                            this.submit();
                        }
                    }}
                />
            </div>
        )
    }

    static propTypes = {
        onSubmitCommand: propTypes.func.isRequired,
    };
}

export default Cli;