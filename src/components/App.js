import React, {Component} from 'react';
import Gameboard from "./gameboard/index";

class App extends Component {
    render() {
        return (
            <Gameboard/>
        );
    }
}

export default App;
