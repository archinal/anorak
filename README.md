# DCLIG
To create a new game you can specify some things in the constructor of a class which
extends DCLIG.

## Actions
The priority order in which actions are chosen is the following:
1. The current scene's actions
1. The inventory actions
1. The custom default actions
1. The default actions

## Command parsing
Takes `verb`, `object`, `indirect object`

## Inventory
Objects as a mapping of the form `ITEM_ID -> {name: ITEM_NAME, quantity: ITEM_QUANTITY}`

# Anorak
The game